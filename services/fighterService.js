const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  getAllFighters() {
    const items = FighterRepository.getAll();
    return items;
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
  createFighter(fighter) {
    if (this.checkExistingName(fighter.name)) {
      throw Error("Fighter with this name already exists");
    }
    if (!fighter.health) {
      fighter.health = 100;
    }
    const item = FighterRepository.create(fighter);
    if (!item) {
      return null;
    }
    return item;
  }
  updateFighter(id, data) {
    if (!this.search({ id })) {
      return null;
    }
    if (data.name && this.checkExistingName(data.name)) {
      throw Error("Fighter with this name already exists");
    }
    const item = FighterRepository.update(id, data);
    return item;
  }
  deleteFighter(id) {
    if (!this.search({ id })) {
      return null;
    }
    const item = FighterRepository.delete(id);
    return item;
  }

  checkExistingName(name) {
    return this.search({ name }) ? true : false;
  }
}

module.exports = new FighterService();
