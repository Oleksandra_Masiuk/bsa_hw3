const { UserRepository } = require("../repositories/userRepository");

class UserService {
  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  getAllUsers() {
    const items = UserRepository.getAll();
    return items;
  }

  createUser(user) {
    if (this.checkExistingEmail(user.email)) {
      throw Error("User with this email already exists");
    }
    if (this.checkExistingPhoneNumber(user.phoneNumber)) {
      throw Error("User with this phoneNumber already exists");
    }
    if (this.checkExistingName(user.firstName, user.lastName)) {
      throw Error("User with this name already exists");
    }
    const item = UserRepository.create(user);
    if (!item) {
      return null;
    }
    return item;
  }
  updateUser(id, data) {
    if (!this.search({ id })) {
      return null;
    }
    if (data.email && this.checkExistingEmail(data.email)) {
      throw Error("User with this email already exists");
    }
    if (data.phoneNumber && this.checkExistingPhoneNumber(data.phoneNumber)) {
      throw Error("User with this phoneNumber already exists");
    }
    const item = UserRepository.update(id, data);
    return item;
  }
  deleteUser(id) {
    if (!this.search({ id })) {
      return null;
    }
    const item = UserRepository.delete(id);
    return item;
  }
  checkExistingEmail(email) {
    return this.search({ email }) ? true : false;
  }
  checkExistingPhoneNumber(phoneNumber) {
    return this.search({ phoneNumber }) ? true : false;
  }
  checkExistingName(firstName, lastName) {
    return this.search({ firstName, lastName }) ? true : false;
  }
}

module.exports = new UserService();
