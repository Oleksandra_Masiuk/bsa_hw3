const { FightRepository } = require("../repositories/fightRepository");

class FightersService {
  search(search) {
    const item = FightRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  createFight(data) {
    const item = FightRepository.create(data);
    if (!item) {
      return null;
    }
    return item;
  }

  updateFight(id, data) {
    if (!this.search({ id })) {
      return null;
    }
    const item = FightRepository.update(id, data);
    return item;
  }

  getAllFights() {
    const items = FightRepository.getAll();
    return items;
  }

  deleteFight(id) {
    if (!this.search({ id })) {
      return null;
    }
    const item = FightRepository.delete(id);
    return item;
  }
}

module.exports = new FightersService();
