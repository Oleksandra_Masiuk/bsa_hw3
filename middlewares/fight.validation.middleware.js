const { fight } = require("../models/fight");

const createFightValid = (req, res, next) => {
  const data = req.body;
  const dataLength = Object.keys(data).length;
  const fightLength = Object.keys(fight).length;
  if (dataLength !== fightLength - 1) {
    res.err = {
      status: 400,
      message: "Cannot create fight with this amount of fields",
    };
    next();
  }
  const validatedFight = validateFight(data);
  if (validatedFight.status !== 200) {
    res.err = validatedFight;
    next();
  }

  next();
};

const updateFightValid = (req, res, next) => {
  const data = req.body;
  if (Object.keys(data).length === 0) {
    res.err = {
      status: 400,
      message: "There should be at least 1 updating field",
    };
    next();
  }
  const validatedFight = validateFight(data);
  if (validatedFight.status !== 200) {
    res.err = validatedFight;
    next();
  }
  next();
};

const validateFight = (data) => {
  for (const key in data) {
    if (key === "id") {
      return { status: 400, message: "There cannot be id in request body" };
    }
    if (data[key].toString().length === 0) {
      return { status: 400, message: "There cannot be empty fields" };
    }
    if (!fight.hasOwnProperty(key)) {
      return { status: 400, message: "There should be only fight's fields" };
    }
  }

  return { status: 200, message: "Fight's data is valid" };
};

exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;
