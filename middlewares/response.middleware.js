const responseMiddleware = (req, res, next) => {
  if (res.data) {
    res.status(200).send(res.data);
  }
  if (res.err) {
    if (res.err.status) {
      res
        .status(res.err.status)
        .send({ error: true, message: res.err.message.toString() });
    } else {
      res.send({ error: true, message: res.err.toString() });
    }
  }
  next();
};

exports.responseMiddleware = responseMiddleware;
