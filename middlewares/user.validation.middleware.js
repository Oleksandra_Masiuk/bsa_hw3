const { user } = require("../models/user");
const createUserValid = (req, res, next) => {
  const data = req.body;
  if (Object.keys(user).length - 1 !== Object.keys(data).length) {
    res.err = {
      status: 400,
      message: "Cannot create user with this amount of fields",
    };
    next();
  }
  const validatedUser = validateUser(data);
  if (validatedUser.status !== 200) {
    res.err = validatedUser;
    next();
  }
  next();
};

const updateUserValid = (req, res, next) => {
  const data = req.body;
  if (Object.keys(data).length === 0) {
    res.err = {
      status: 400,
      message: "There should be at least 1 updating field",
    };
    next();
  }
  const validatedUser = validateUser(data);
  if (validatedUser.status !== 200) {
    res.err = validatedUser;
    next();
  }
  next();
};

const validateUser = (data) => {
  for (const key in data) {
    if (key === "id") {
      return { status: 400, message: "There cannot be id in request body" };
    }
    if (data[key].toString().length === 0) {
      return { status: 400, message: "There cannot be empty fields" };
    }
    if (!user.hasOwnProperty(key)) {
      return { status: 400, message: "There should be only user's fields" };
    }
    if (key === "email") {
      const splittedEmail = data[key].split("@gmail.com");
      if (
        !(
          splittedEmail &&
          splittedEmail.length == 2 &&
          splittedEmail[1].length === 0 &&
          splittedEmail[0].length > 0
        )
      ) {
        return {
          status: 400,
          message: "There can be only valid gmail email",
        };
      }
    }
    if (key === "phoneNumber") {
      if (!/(\+380)[0-9]{9}/g.test(data[key]) || data[key].length !== 13)
        return {
          status: 400,
          message: "Phone number should be in a format: +380xxxxxxxxx",
        };
    }
    if (key === "password" || key === "firstName" || key === "lastName") {
      if (typeof data[key] !== "string") {
        return {
          status: 400,
          message: `User's ${key} should be a string`,
        };
      }
      if (data[key].length < 3) {
        return {
          status: 400,
          message: `User's ${key} should be at least 3 symbols long`,
        };
      }
    }
    if (
      (key === "firstName" || key === "lastName") &&
      !/^[A-Za-z\-\s]+$/.test(data[key])
    ) {
      return {
        status: 400,
        message: `${key} should contain only letters, space or symbol -`,
      };
    }
  }
  return { status: 200, message: "User's data is valid" };
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
