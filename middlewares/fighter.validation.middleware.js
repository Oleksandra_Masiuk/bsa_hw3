const { fighter } = require("../models/fighter");

const createFighterValid = (req, res, next) => {
  const data = req.body;
  const dataLength = Object.keys(data).length;
  const fighterLength = Object.keys(fighter).length;
  if (data.hasOwnProperty("health") && dataLength !== fighterLength - 1) {
    res.err = {
      status: 400,
      message: "Cannot create fighter with this amount of fields",
    };
    next();
  } else if (
    !data.hasOwnProperty("health") &&
    dataLength !== fighterLength - 2
  ) {
    res.err = {
      status: 400,
      message: "Cannot create fighter with this amount of fields",
    };
    next();
  }
  const validatedFighter = validateFighter(data);
  if (validatedFighter.status !== 200) {
    res.err = validatedFighter;
    next();
  }

  next();
};

const updateFighterValid = (req, res, next) => {
  const data = req.body;
  if (Object.keys(data).length === 0) {
    res.err = {
      status: 400,
      message: "There should be at least 1 updating field",
    };
    next();
  }
  const validatedFighter = validateFighter(data);
  if (validatedFighter.status !== 200) {
    res.err = validatedFighter;
    next();
  }
  next();
};

const validateFighter = (data) => {
  for (const key in data) {
    if (key === "id") {
      return { status: 400, message: "There cannot be id in request body" };
    }
    if (data[key].toString().length === 0) {
      return { status: 400, message: "There cannot be empty fields" };
    }
    if (!fighter.hasOwnProperty(key)) {
      return { status: 400, message: "There should be only fighter's fields" };
    }
    if (key === "name") {
      if (typeof data[key] !== "string") {
        return {
          status: 400,
          message: `Fighter's ${key} should be a string`,
        };
      }
      if (data[key].length < 3) {
        return {
          status: 400,
          message: `Fighter's ${key} should be at least 3 symbols long`,
        };
      }
    }
    if (
      (key === "power" || key === "defense" || key === "health") &&
      typeof data[key] !== "number"
    ) {
      return { status: 400, message: `Fighter's ${key} should be a number` };
    }
    if (key === "power" && (1 >= data[key] || data[key] >= 100)) {
      return {
        status: 400,
        message: "Fighter's power should be more than 1 and less then 100",
      };
    }
    if (key === "defense" && (1 >= data[key] || data[key] >= 10)) {
      return {
        status: 400,
        message: "Fighter's defense should be more than 1 and less then 10",
      };
    }
    if (key === "health" && (80 >= data[key] || data[key] >= 120)) {
      return {
        status: 400,
        message: "Fighter health should be more than 80 and less then 120",
      };
    }
  }

  return { status: 200, message: "Fighter's data is valid" };
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
