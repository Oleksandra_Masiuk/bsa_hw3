const { Router } = require("express");
const FightService = require("../services/fightService");
const {
  createFightValid,
  updateFightValid,
} = require("../middlewares/fight.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.get(
  "",
  (req, res, next) => {
    const fights = FightService.getAllFights();
    res.data = fights;
    next();
  },
  responseMiddleware
);

router.get(
  "/:id",
  (req, res, next) => {
    const fight = FightService.search({ id: req.params.id });
    if (fight) {
      res.data = fight;
    } else {
      res.err = { status: 404, message: "Fight not found" };
    }
    next();
  },
  responseMiddleware
);
router.post(
  "",
  createFightValid,
  (req, res, next) => {
    if (!res.err) {
      try {
        const fight = FightService.createFight(req.body);
        if (fight) {
          res.data = fight;
        } else {
          res.err = { status: 404, message: "Fight not found" };
        }
      } catch (err) {
        res.err = { status: 400, message: err.toString() };
      }
    }
    next();
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateFightValid,
  function (req, res, next) {
    if (!res.err) {
      try {
        const fight = FightService.updateFight(req.params.id, req.body);
        if (fight) {
          res.data = fight;
        } else {
          res.err = { status: 404, message: "Fight not found" };
        }
      } catch (err) {
        res.err = { status: 400, message: err.toString() };
      }
    }
    next();
  },
  responseMiddleware
);

router.delete(
  "/:id",
  function (req, res, next) {
    const fight = FightService.deleteFight(req.params.id);
    if (fight) {
      res.data = fight;
    } else {
      res.err = { status: 404, message: "Fight not found" };
    }
    next();
  },
  responseMiddleware
);
module.exports = router;
